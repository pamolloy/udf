use semver::Version;
use std::path::PathBuf;
use structopt::StructOpt;

// https://stackoverflow.com/questions/50072055/converting-unix-timestamp-to-readable-time-string-in-rust

#[derive(StructOpt, Debug)]
#[structopt(name = "udf", author, about)]
enum Opt {
    #[structopt(about = "Convert data into hex to be consumed by `bootgen")]
    Pack {
        #[structopt(short, long)]
        pmu_version: Version,
        #[structopt(short, long)]
        fsbl_version: Version,
        #[structopt(short, long)]
        atf_version: Version,
        #[structopt(short, long)]
        u_boot_version: Version,
        #[structopt(short, long)]
        build_time: u64,
    },
    #[structopt(about = "Read data from a `BOOT.bin` and output JSON")]
    Unpack { boot_binary: PathBuf },
}

fn main() {
    let opt = Opt::from_args();
    println!("{:#?}", opt);
}
