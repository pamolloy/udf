# Xilinx boot binary UDF

The Xilinx boot binary (`BOOT.bin`) includes several user defined fields (UDF),
including one in the boot header and one with the authentication certificate.

```bash
$ udf pack -b $(date +%s) -p 1.0.2 -f 1.0.1 -a xilinx-v2018.2 -u procyteone-2019.01.07 | tee udf_bh.hex
49445858c19e5d5c010000000200010000000100e20702000000e3070100070000000000
```

```bash
$ udf unpack BOOT.bin
{
	"build_time": 1549557012,
	"pmu_version": "1.0.2",
	"fsbl_version": "1.0.1",
	"atf_version": "2018.2.0",
	"u_boot_version": "2019.1.7"
}
```
